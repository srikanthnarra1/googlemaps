/**
 * OpenLayers 3 Layer Switcher Control.
 * See [the examples](./examples) for usage.
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object} opt_options Control options, extends olx.control.ControlOptions adding:
 *                              **`tipLabel`** `String` - the button tooltip.
 */
ol.control.BaseMapSwitcher = function (opt_options) {

    var options = opt_options || {};
    var tipLabel = options.tipLabel ?
      options.tipLabel : 'Base Map';
    this.mapListeners = [];

    this.hiddenClassName = 'ol-unselectable ol-control basemap-switcher';
    if (ol.control.BaseMapSwitcher.isTouchDevice_()) {
        this.hiddenClassName += ' touch';
    }
    this.shownClassName = this.hiddenClassName + ' shown';

    var element = document.createElement('div');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.setAttribute('title', tipLabel);
    element.appendChild(button);

    this.panel = document.createElement('div');
    this.panel.className = 'panel';
    element.appendChild(this.panel);
    ol.control.BaseMapSwitcher.enableTouchScroll_(this.panel);

    var this_ = this;

    button.onmouseover = function (e) {
        this_.showPanel();
    };

    button.onclick = function (e) {
        e = e || window.event;
        this_.showPanel();
        e.preventDefault();
    };

    this_.panel.onmouseout = function (e) {
        e = e || window.event;
        if (!this_.panel.contains(e.toElement || e.relatedTarget)) {
            this_.hidePanel();
        }
    };

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.BaseMapSwitcher, ol.control.Control);

/**
 * Show the layer panel.
 */
ol.control.BaseMapSwitcher.prototype.showPanel = function () {
    if (this.element.className != this.shownClassName) {
        this.element.className = this.shownClassName;
        this.renderPanel();
    }
};

/**
 * Hide the layer panel.
 */
ol.control.BaseMapSwitcher.prototype.hidePanel = function () {
    if (this.element.className != this.hiddenClassName) {
        this.element.className = this.hiddenClassName;
    }
};

/**
 * Re-draw the layer panel to represent the current state of the layers.
 */
ol.control.BaseMapSwitcher.prototype.renderPanel = function () {

    this.ensureTopVisibleBaseLayerShown_();
    while (this.panel.firstChild) {
        this.panel.removeChild(this.panel.firstChild);
    }
    var ul = document.createElement('ul');
    this.panel.appendChild(ul);  

    var li = document.createElement('li');
    var label = document.createElement('label');
    li.className = 'layer';
    label.innerHTML = "Google";
    li.appendChild(label);
    li.onclick = function () {
        var event = new CustomEvent("basemapChanged", { "detail": { mapSelected: "Roads" } });
        // Dispatch/Trigger/Fire the event
        document.dispatchEvent(event);
    }

    ul.appendChild(li);

    var li = document.createElement('li');
    var label = document.createElement('label');
    li.className = 'layer';
    label.innerHTML = "OS Map";
    li.appendChild(label);
    li.onclick = function () {
        var event = new CustomEvent("basemapChanged", { "detail": { mapSelected: "OS Map" } });
        // Dispatch/Trigger/Fire the event
        document.dispatchEvent(event);
    }
    ul.appendChild(li);
};

/**
 * Set the map instance the control is associated with.
 * @param {ol.Map} map The map instance.
 */
ol.control.BaseMapSwitcher.prototype.setMap = function (map) {
    // Clean up listeners associated with the previous map
    for (var i = 0, key; i < this.mapListeners.length; i++) {
        this.getMap().unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
    // Wire up listeners etc. and store reference to new map
    ol.control.Control.prototype.setMap.call(this, map);
    if (map) {
        var this_ = this;
        this.mapListeners.push(map.on('pointerdown', function () {
            this_.hidePanel();
        }));
        this.renderPanel();
    }
};

/**
 * Ensure only the top-most base layer is visible if more than one is visible.
 * @private
 */
ol.control.BaseMapSwitcher.prototype.ensureTopVisibleBaseLayerShown_ = function () {
    var lastVisibleBaseLyr;
    ol.control.BaseMapSwitcher.forEachRecursive(this.getMap(), function (l, idx, a) {
        if (l.get('type') === 'base' && l.getVisible()) {
            lastVisibleBaseLyr = l;
        }
    });
    if (lastVisibleBaseLyr) this.setVisible_(lastVisibleBaseLyr, true);
};

/**
 * Toggle the visible state of a layer.
 * Takes care of hiding other layers in the same exclusive group if the layer
 * is toggle to visible.
 * @private
 * @param {ol.layer.Base} The layer whos visibility will be toggled.
 */
ol.control.BaseMapSwitcher.prototype.setVisible_ = function (lyr, visible) {
    var map = this.getMap();
    lyr.setVisible(visible);
    if (visible && lyr.get('type') === 'base') {
        // Hide all other base layers regardless of grouping
        ol.control.BaseMapSwitcher.forEachRecursive(map, function (l, idx, a) {
            if (l != lyr && l.get('type') === 'base') {
                l.setVisible(false);
            }
        });
    }
};

/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Base} lyr Layer to be rendered (should have a title property).
 * @param {Number} idx Position in parent group list.
 */
ol.control.BaseMapSwitcher.prototype.renderLayer_ = function (lyr, idx) {


    var li = document.createElement('li');

    
    return div;

    var this_ = this;

    var li = document.createElement('li');

    var lyrTitle = lyr.get('title');
    var lyrId = ol.control.BaseMapSwitcher.uuid();

    var label = document.createElement('label');

    if (lyr.getLayers && !lyr.get('combine')) {

        li.className = 'group';
        label.innerHTML = lyrTitle;
        li.appendChild(label);
        var ul = document.createElement('ul');
        li.appendChild(ul);

        this.renderLayers_(lyr, ul);

    } else {

        li.className = 'layer';
        var input = document.createElement('input');
        if (lyr.get('type') === 'base') {
            input.type = 'radio';
            input.name = 'base';
        } else {
            input.type = 'checkbox';
        }
        input.id = lyrId;
        input.checked = lyr.get('visible');
        input.onchange = function (e) {
            this_.setVisible_(lyr, e.target.checked);
        };
        var table = document.createElement('table');
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        td.style.width = "30px";
        td.appendChild(input);

        //li.appendChild(input);
        var legendImg = document.createElement('img');
        legendImg.style.marginTop = "-32px";
        legendImg.src = lyr.getSource().getUrl() + "&REQUEST=GetLegendGraphic&FORMAT=image/png&SERVICE=WMS&VERSION=1.3.0&LAYER=" + lyr.getSource().getParams()['LAYERS'];
        var imgCont = document.createElement('div');
        imgCont.style.overflow = 'hidden';
        imgCont.appendChild(legendImg);

        label.htmlFor = lyrId;
        label.innerHTML = lyrTitle;
        var td1 = document.createElement('td');
        td1.appendChild(label);
        tr.appendChild(td);
        tr.appendChild(td1);
        table.appendChild(tr);

        var tr1 = document.createElement('tr');
        var td2 = document.createElement('td');
        td2.appendChild(imgCont);
        td2.setAttribute('colspan', '2');
        td2.style.textAlign = 'center';
        tr1.appendChild(td2);
        table.appendChild(tr1);
        //li.appendChild(label);
        li.appendChild(table);
    }

    return li;

};

/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Group} lyr Group layer whos children will be rendered.
 * @param {Element} elm DOM element that children will be appended to.
 */
ol.control.BaseMapSwitcher.prototype.renderLayers_ = function (lyr, elm) {
    var lyrs = lyr.getLayers().getArray().slice().reverse();
    for (var i = 0, l; i < lyrs.length; i++) {
        l = lyrs[i];
        if (l.get('title')) {
            elm.appendChild(this.renderLayer_(l, i));
        }
    }
};

/**
 * **Static** Call the supplied function for each layer in the passed layer group
 * recursing nested groups.
 * @param {ol.layer.Group} lyr The layer group to start iterating from.
 * @param {Function} fn Callback which will be called for each `ol.layer.Base`
 * found under `lyr`. The signature for `fn` is the same as `ol.Collection#forEach`
 */
ol.control.BaseMapSwitcher.forEachRecursive = function (lyr, fn) {
    lyr.getLayers().forEach(function (lyr, idx, a) {
        fn(lyr, idx, a);
        if (lyr.getLayers) {
            ol.control.BaseMapSwitcher.forEachRecursive(lyr, fn);
        }
    });
};

/**
 * Generate a UUID
 * @returns {String} UUID
 *
 * Adapted from http://stackoverflow.com/a/2117523/526860
 */
ol.control.BaseMapSwitcher.uuid = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

/**
* @private
* @desc Apply workaround to enable scrolling of overflowing content within an
* element. Adapted from https://gist.github.com/chrismbarr/4107472
*/
ol.control.BaseMapSwitcher.enableTouchScroll_ = function (elm) {
    if (ol.control.BaseMapSwitcher.isTouchDevice_()) {
        var scrollStartPos = 0;
        elm.addEventListener("touchstart", function (event) {
            scrollStartPos = this.scrollTop + event.touches[0].pageY;
        }, false);
        elm.addEventListener("touchmove", function (event) {
            this.scrollTop = scrollStartPos - event.touches[0].pageY;
        }, false);
    }
};

/**
 * @private
 * @desc Determine if the current browser supports touch events. Adapted from
 * https://gist.github.com/chrismbarr/4107472
 */
ol.control.BaseMapSwitcher.isTouchDevice_ = function () {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch (e) {
        return false;
    }
};

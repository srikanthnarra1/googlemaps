ol.control.IdentifyFeature = function(opt_options) {

    var options = opt_options || {};
    var tipLabel = options.tipLabel ?
      options.tipLabel : 'Identify';
    this.mapListeners = [];
	this.allLayers = [];
	this.identifyLayers = ['Sewer','Manhole','Hydrant','Sewage Discharge','Mainspipe','Misc Assets','Water Air Valve','Water Valve','Water Joint','Sewer Air Valve','Sewer Valve','Sewer Joint'];
	this.result = [];
	this.resultFlag=0;
	this.clickedLocation = null;
    this.hiddenClassName = 'ol-unselectable ol-control identify-feature';
    if (ol.control.IdentifyFeature.isTouchDevice_()) {
        this.hiddenClassName += ' touch';
    }
    this.shownClassName = this.hiddenClassName + ' shown';

    var element = document.createElement('div');
    element.className = this.hiddenClassName;

    var button = document.createElement('button');
    button.setAttribute('title', tipLabel);
	button.innerHTML='i';
    element.appendChild(button);

    this.panel = document.createElement('div');
    this.panel.className = 'panel';
    element.appendChild(this.panel);
    ol.control.IdentifyFeature.enableTouchScroll_(this.panel);

    var this_ = this;

    button.onclick = function(e) {
        e = e || window.event;
        this_.activateControl();
        e.preventDefault();
    };    

    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });
	this.createUI();

};

ol.inherits(ol.control.IdentifyFeature, ol.control.Control);

/**
 * Activate the map click event.
 */
ol.control.IdentifyFeature.prototype.activateControl = function() {
	var map = this.getMap();
	if(drawFeature){
		map.removeInteraction(drawFeature.draw);
	}
	if (map) {
        var this_ = this;
        this.mapListeners.push(map.on('singleclick', function(evt) {
			this_.clickedLocation = evt.pixel;
            this_.getFeatureInfo(evt.pixel);
        }));
    }    
};

ol.control.IdentifyFeature.prototype.getFeatureInfo = function(coordinate) {
    var map = this.getMap();
	var size = map.getSize();
	var view = map.getView();
	var viewResolution = view.getResolution();
	var params=[
		'SERVICE=WMS',
		'VERSION=1.3.0',
		'REQUEST=GetFeatureInfo',
		'FORMAT=image/png',
		'TRANSPARENT=true',
		'INFO_FORMAT=application/json',
		'WIDTH='+size[0],
		'HEIGHT='+size[1],
		'I='+coordinate[0],
		'J='+coordinate[1],
		'CRS='+view.getProjection().getCode(),
		'STYLES=',
		'BBOX='+view.calculateExtent(size).join(','),
		'EXCEPTIONS=application/json'
	];
	var layerNames=[];
	var url='';
	for(var x=0;x<this.allLayers.length;x++){
		var lyr = this.allLayers[x];
		if(lyr instanceof ol.layer.Tile && lyr.getVisible()){
			url = lyr.getSource().getUrls()[0]+"?";
			layerNames.push(lyr.getSource().getParams()["LAYERS"]);			
		}
	}	
	url += params.join('&')+'&QUERY_LAYERS='+layerNames+'&LAYERS='+layerNames+'&FEATURE_COUNT=1';
	$.ajax({
			url: url,//+'&FI_POINT_TOLERANCE=16&FI_LINE_TOLERANCE=8&FI_POLYGON_TOLERANCE=4',	
			dataType:'application/json',
			success:function(r){
				console.log(r);
			}.bind(this),
			statusCode:{
				200:function(r){
					var t;
					eval('t='+r.responseText);					
					if(t.features && t.features.length>0){
						console.log(t.features);
						this.setContent(t.features);
						this.show();
					}
				}.bind(this)
			}
		}				
	);
};
ol.control.IdentifyFeature.prototype.createUI = function() {
    this.iwContainer = document.createElement('div');
    this.iwContainer.className = 'info-window-container';
	
	var iwHeader = document.createElement('div');
	iwHeader.className = 'info-window-header';
	iwHeader.innerHTML='Info Window';
	this.iwContainer.appendChild(iwHeader);
	
	var iwCloseButton = document.createElement('div');
    iwCloseButton.className = 'info-window-close-button';
	iwHeader.appendChild(iwCloseButton);
	var button = document.createElement('button');
    button.setAttribute('title', 'close');
	button.innerHTML='x';
    iwCloseButton.appendChild(button);
	var this_ = this;
	button.onclick = function(e) {
        e = e || window.event;
        this_.hide();
        e.preventDefault();
    };
	
	this.iwContent = document.createElement('div');
    this.iwContent.className = 'info-window-content';
	this.iwContainer.appendChild(this.iwContent);
	
	this.iwAnchor = document.createElement('div');
    this.iwAnchor.className = 'info-window-anchor';
	this.iwContainer.appendChild(this.iwAnchor);
	
	document.body.appendChild(this.iwContainer);
};

ol.control.IdentifyFeature.prototype.show = function() {
    this.iwContainer.style.display='block';
};

ol.control.IdentifyFeature.prototype.hide = function() {
    this.iwContainer.style.display='none';
};

ol.control.IdentifyFeature.prototype.setContent = function(data) {
    this.iwContent.innerHTML='';
	var content="";
	for(var x=0;x<data.length;x++){
		if(data[x].id){
			content +="<div class='info-window-content header'>"+data[x].id+"</div><div class='hzLine'></div>";
			content +="<div><table class='attrTable' cellpadding='0px' cellspacing='0px'><tbody>";
			for(var y in data[x].properties){
				if(data[x].properties.hasOwnProperty(y)){
					//var attr = data[x].Layer.Feature.Attribute[y];
					var val = data[x].properties[y];
					if(!isNaN(val)){
						var tempVal = val*1;
						val = tempVal.toFixed(2);
					}
					content +="<tr valign='top'><td class='attrName'>"+y+"</td><td class='attrValue'>"+val+"</td></tr>";
				}
			}
			content +="</tbody></table></div>";
		}
	}
	this.iwContent.innerHTML=content;
	
	this.iwContainer.style.display='block';
	var h = this.iwContainer.clientHeight;
	var w = this.iwContainer.clientWidth;
	var mapSize = this.getMap().getSize();
	
	var vclassName="top";
	var hclassName="left";
	var anchorSize = 16;
	if (this.clickedLocation[1] < mapSize[1]/2) {
		this.iwContainer.style.top= (this.clickedLocation[1]+anchorSize)+"px";
    }
	else {
		this.iwContainer.style.top= (this.clickedLocation[1]-h-anchorSize)+"px";
		vclassName="bottom";
    }
	
	if (this.clickedLocation[0] < mapSize[0]/2 ) {
		this.iwContainer.style.left= (this.clickedLocation[0]+16)+"px";
    }
	else {
		this.iwContainer.style.left= (this.clickedLocation[0]-w-anchorSize)+"px";
		hclassName="right";
    }
};

/**
 * Activate the map click event.
 */
ol.control.IdentifyFeature.prototype.deactivateControl = function() {
    for (var i = 0, key; i < this.mapListeners.length; i++) {
        this.getMap().unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
};

/**
 * Set the map instance the control is associated with.
 * @param {ol.Map} map The map instance.
 */
ol.control.IdentifyFeature.prototype.setMap = function(map) {
    // Clean up listeners associated with the previous map
    for (var i = 0, key; i < this.mapListeners.length; i++) {
        this.getMap().unByKey(this.mapListeners[i]);
    }
    this.mapListeners.length = 0;
	var _this = this;
	var allLayers = [];
	var mapLayers = map.getLayers().getArray();
	mapLayers.forEach(function (layer, i) {
		if (layer instanceof ol.layer.Group && layer.getVisible() == true ) {
			layer.getLayers().getArray().forEach(function(sublayer, j, layers) {
				if(_this.identifyLayers.indexOf(sublayer.get('title')) >-1){
					allLayers.push(sublayer);
				}				
			})
		} else if ( !(layer instanceof ol.layer.Group) && layer.getVisible() == true ) {
				if(_this.identifyLayers.indexOf(layer.get('title')) >-1){
					allLayers.push(layer);
				}
		}
	});
	this.allLayers=allLayers;
    // Wire up listeners etc. and store reference to new map
    ol.control.Control.prototype.setMap.call(this, map);
};

/**
 * Generate a UUID
 * @returns {String} UUID
 *
 * Adapted from http://stackoverflow.com/a/2117523/526860
 */
ol.control.IdentifyFeature.uuid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

/**
* @private
* @desc Apply workaround to enable scrolling of overflowing content within an
* element. Adapted from https://gist.github.com/chrismbarr/4107472
*/
ol.control.IdentifyFeature.enableTouchScroll_ = function(elm) {
   if(ol.control.IdentifyFeature.isTouchDevice_()){
       var scrollStartPos = 0;
       elm.addEventListener("touchstart", function(event) {
           scrollStartPos = this.scrollTop + event.touches[0].pageY;
       }, false);
       elm.addEventListener("touchmove", function(event) {
           this.scrollTop = scrollStartPos - event.touches[0].pageY;
       }, false);
   }
};

/**
 * @private
 * @desc Determine if the current browser supports touch events. Adapted from
 * https://gist.github.com/chrismbarr/4107472
 */
ol.control.IdentifyFeature.isTouchDevice_ = function() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch(e) {
        return false;
    }
};
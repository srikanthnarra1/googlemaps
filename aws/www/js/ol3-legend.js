ol.control.Legend = function(opt_options) {
	var options = opt_options || {};
	this.layersInfo = options.layersInfo;
	this.domId = options.domId;
	//ol.control.Legend.enableTouchScroll_(this.domId);
	this.renderLegend();
	var this_=this;
	for(var x=0;x<this.layersInfo.length;x++){
		this.layersInfo[x].layer.on('change:visible',this_.renderLegend, this_);
	}
};

ol.inherits(ol.control.Legend, ol.control.Control);

/**
 * Render all layers that are children of a group.
 * @private
 * @param {ol.layer.Base} lyr Layer to be rendered (should have a title property).
 * @param {Number} idx Position in parent group list.
 */
ol.control.Legend.prototype.renderLegend = function() {
	
	var legendDomNode = document.getElementById(this.domId);
	legendDomNode.innerHTML='';
	var table = document.createElement('table');

	for(var x=0;x<this.layersInfo.length;x++){
		
		var lyrInfo = this.layersInfo[x];
		var isVisible = lyrInfo.layer.getVisible();
		if(isVisible){
			var lyrSource = lyrInfo.layer.getSource();
			
			var legendImg = document.createElement('img');
			if(lyrSource.getUrls()){
				legendImg.src=lyrInfo.url+"?REQUEST=GetLegendGraphic&FORMAT=image/png&SERVICE=WMS&VERSION=1.3.0&LAYER="+lyrInfo.name;
			}
			var imgCont = document.createElement('div');
			imgCont.setAttribute("class","layer-legend-label");
			imgCont.appendChild(legendImg);
			
			var label = document.createElement('label');
			label.setAttribute("class","layer-legend-label");
			label.htmlFor = lyrInfo.label;
			label.innerHTML = lyrInfo.label;
			
			var layerLabelRow = document.createElement('tr');
			var layerLabelCell = document.createElement('td');
			layerLabelCell.appendChild(label);
			layerLabelRow.appendChild(layerLabelCell);
			table.appendChild(layerLabelRow);
			
			var layerLegendRow = document.createElement('tr');
			var layerLegendCell = document.createElement('td');
			layerLegendCell.appendChild(imgCont);
			layerLegendRow.appendChild(layerLegendCell);
			table.appendChild(layerLegendRow);
		}
	}
	legendDomNode.innerHTML = table.outerHTML;
};

/**
 * Generate a UUID
 * @returns {String} UUID
 *
 * Adapted from http://stackoverflow.com/a/2117523/526860
 */
ol.control.Legend.uuid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

/**
* @private
* @desc Apply workaround to enable scrolling of overflowing content within an
* element. Adapted from https://gist.github.com/chrismbarr/4107472
*/
ol.control.Legend.enableTouchScroll_ = function(elm) {
   if(ol.control.Legend.isTouchDevice_()){
       var scrollStartPos = 0;
       elm.addEventListener("touchstart", function(event) {
           scrollStartPos = this.scrollTop + event.touches[0].pageY;
       }, false);
       elm.addEventListener("touchmove", function(event) {
           this.scrollTop = scrollStartPos - event.touches[0].pageY;
       }, false);
   }
};

/**
 * @private
 * @desc Determine if the current browser supports touch events. Adapted from
 * https://gist.github.com/chrismbarr/4107472
 */
ol.control.Legend.isTouchDevice_ = function() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch(e) {
        return false;
    }
};
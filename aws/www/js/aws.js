proj4.defs("EPSG:27700", '+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717' +
    ' +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs');

/* limit the bounds of the UK projection for the graticule */
ol.proj.get('EPSG:27700').setExtent([0, 0, 800000, 1400000]);
ol.proj.get('EPSG:27700').setWorldExtent([-10, 49, 6, 63]);
ol.proj.get('EPSG:27700').setGlobal(false);
ol.proj.get('EPSG:27700').setGetPointResolution(function (resolution) { return resolution; });

var openSpaceOl3 = new OpenSpaceOl3('3AE71B01F7034829E0530C6CA40A35B3', "https://geosolutions.cognizant.com/", OpenSpaceOl3.ALL_LAYERS);
var map = null;
var gmap = null;
var baseMapType = "OSM";
var extent = null;
var projection = null;
var mapDetails = sessionStorage.getItem("mapDetails");
if (mapDetails != undefined && mapDetails != null) {
    var json = JSON.parse(mapDetails);
    baseMapType = json.baseMapType;
    extent = json.extent;
    projection = json.projection;
}


if (baseMapType == "Roads") {
    setGmap();
}
else {

}

function setGmap(){
    gmap = new google.maps.Map(document.getElementById('gmap'), {
        disableDefaultUI: true,
        keyboardShortcuts: false,
        draggable: false,
        disableDoubleClickZoom: true,
        scrollwheel: false,
        streetViewControl: true
    });
}

var awsLayers = new ol.layer.Group({
    title: 'Overlays',
    layers: [
        new ol.layer.Image({
            title: 'UK Border',
            source: new ol.source.ImageWMS({
                url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK.qgs',
                params: { 'LAYERS': 'Border' },
                serverType: 'qgis'
            })
        }),
        new ol.layer.Image({
            title: 'Sewer',
            source: new ol.source.ImageWMS({
                url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
                params: { 'LAYERS': 'Sewer' },
                serverType: 'qgis'
            })
        }),
        new ol.layer.Image({
            title: 'Manhole',
            source: new ol.source.ImageWMS({
                url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
                params: { 'LAYERS': 'Manhole' },
                serverType: 'qgis'
            })
        }),
        new ol.layer.Image({
            title: 'Hydrant',
            source: new ol.source.ImageWMS({
                url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
                params: { 'LAYERS': 'Hydrant' },
                serverType: 'qgis'
            })
        })
    ]
});

var view = new ol.View({    
    maxZoom: 21,      
});


var view2 = new ol.View({
    projection: openSpaceOl3.getProjection(),
    center: [588090.0000000002, 187199.99999999997], // OS coords
    resolutions: openSpaceOl3.getResolutions(),
    resolution: 2,
    //extent: ol.proj.get('EPSG:27700').getExtent()
});
view.on('change:center', function () {
    if (gmap != null) {
        var center = ol.proj.transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
        gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
    }
});
view.on('change:resolution', function () {
    if (gmap != null) {
        gmap.setZoom(view.getZoom());
    }
});

var olMapDiv = document.getElementById('olmap');


if (baseMapType == "Roads") {
    map = new ol.Map({
        layers: [awsLayers],
        interactions: ol.interaction.defaults({
            altShiftDragRotate: false,
            dragPan: false,
            rotate: false
        }).extend([new ol.interaction.DragPan({ kinetic: null })]),
        target: olMapDiv
    });
    map.setView(view);
}
else {
    map = new ol.Map({
        layers: [openSpaceOl3.getLayer(), awsLayers],
        interactions: ol.interaction.defaults({
            altShiftDragRotate: false,
            dragPan: false,
            rotate: false
        }).extend([new ol.interaction.DragPan({ kinetic: null })]),
        target: olMapDiv
    });
    map.setView(view2);
}


if (extent != null) {
  /*  var mapProj = map.getView().getProjection().mb    
    if (mapProj != projection) {
        var newExt = [extent[0], extent[1], extent[2], extent[3]];
        extent = ol.proj.transformExtent(newExt, projection, map.getView().getProjection().mb);
       
    }
    map.getView().fit(extent, map.getSize());  */
}

if (gmap != null) {
    olMapDiv.parentNode.removeChild(olMapDiv);
    gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(olMapDiv);
}
else {
    
    document.getElementById("gmap") .style.display = "none";
}

var layerSwitcher = new ol.control.LayerSwitcher({
    tipLabel: 'Legend' // Optional label for button
});
map.addControl(layerSwitcher);

var identifyFeature = new ol.control.IdentifyFeature({
    tipLabel: 'Identify' // Optional label for button
});
map.addControl(identifyFeature);

var baseMapSwitcher = new ol.control.BaseMapSwitcher({
    tipLabel: 'Base Map' // Optional label for button
});
map.addControl(baseMapSwitcher);


document.addEventListener("basemapChanged", function (e) {
    switch (e.detail.mapSelected) {
        case "Roads":
            baseMapType = "Roads";
            setLocalStorage(baseMapType);
            break;
        case "OS Map":
            baseMapType = "OSM";
            setLocalStorage(baseMapType);
        default:
            break;
    }
});

function setLocalStorage(baseMapType) {
    var mapDetails = { baseMapType: baseMapType, extent: map.getView().calculateExtent(map.getSize()), projection: map.getView().getProjection().mb }
    sessionStorage.setItem("mapDetails", JSON.stringify(mapDetails));
    location.reload();
}


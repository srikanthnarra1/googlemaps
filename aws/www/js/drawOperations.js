var polygon;
var tempfeatureOverlay;
var selectionInteraction;
var highlightLayer = null;
function CreateFeatures(){
	var features = new ol.Collection();
	var featureOverlay= new ol.layer.Vector(
		{
			source: new ol.source.Vector({features: features}),
			style: new ol.style.Style({
				fill: new ol.style.Fill({
					color: 'rgba(255, 255, 255, 0.2)'
				}),
				stroke: new ol.style.Stroke({
					color: '#ffcc33',
					width: 2
				}),
				image: new ol.style.Circle({
					radius: 7,
					fill: new ol.style.Fill({
					  color: '#ffcc33'
					})
				})
			})
		}
	);
	featureOverlay.setMap(map);
	tempfeatureOverlay=featureOverlay;
	return features;
}

function addInteraction(evtType) {

	if(tempfeatureOverlay !=null){
		tempfeatureOverlay.getSource().clear();
	}
	if(selectionInteraction)
		map.removeInteraction(selectionInteraction);
	if(evtType == 'Rectangle'){
		selectionInteraction = new ol.interaction.Draw({
			features: CreateFeatures(),
			type: 'Circle',
			geometryFunction: ol.interaction.Draw.createBox()
		});
	}
	else{
		selectionInteraction = new ol.interaction.Draw({
			features: CreateFeatures(),
			type: evtType
		});
	}
	map.addInteraction(selectionInteraction);
	typeSelect=$(('#' + evtType ));
	selectionInteraction.on('drawend', function(event) {
		polygon = event.feature.getGeometry();		
		map.removeInteraction(selectionInteraction);			
		getFeatureInfo1(polygon);
	});
}
  
function ClearGeometry()
{
	try{
		if(highlightLayer==null){
			return;
		}
		tempfeatureOverlay.getSource().clear();
		if(highlightLayer != null){
			map.removeLayer(highlightLayer);
		}
		$(list2).jqGrid("GridUnload");	
	}
	catch(ex){
		console.log(ex);		
	}  
}


function getFeatureInfo1(coordinate){
	var size = map.getSize();
	var view = map.getView();
	var viewResolution = view.getResolution();
	
	var layerNames=[];
	for(var x=0;x<layerData.data.length;x++){
		if(layerData.data[x].layers.length>0){
			for(var y=0;y<layerData.data[x].layers.length;y++){
				var data = layerData.data[x].layers[y];
				if(data.layer.getVisible()){
					layerNames.push(data.name);
				}
			}	
		}
	}
	var transformedGeometry;
	if(googleInitialized){
		transformedGeometry = coordinate.clone().transform('EPSG:3857','EPSG:27700');
	}
	else{
		transformedGeometry = coordinate.clone();
	}
	var drawArea = new ol.format.WKT().writeFeature(new ol.Feature({geometry: transformedGeometry}));
	
	var url= layerData.baseUrl + "GEOSPATIAL/wfs?service=WFS&version=1.0.0&request=GetFeature&typeName=";
	url +=layerNames.join(',')+'&maxFeatures=1000'; 
	url += "&outputFormat=application/json";
	url += "&CQL_FILTER=INTERSECTS(SHAPE,"+ drawArea +")";	
	
	console.log(url);
	$.ajax(
		{
			url: url,
			dataType:'application/json',
			success:function(r){
				//console.log(r);
			},
			statusCode:{
				200:function(r){
					var t;
					var featureprj;
					eval('t='+r.responseText);					
					if(t.features && t.features.length>0){
						if(googleInitialized){
							for(var x=0;x<t.features.length;x++){
								for(var y=0;y<t.features[x].geometry.coordinates.length;y++){
									if(t.features[x].geometry.type=="LineString"){
										t.features[x].geometry.coordinates[y] = ol.proj.transform(t.features[x].geometry.coordinates[y], 'EPSG:27700', 'EPSG:3857');
									}
									else{
										t.features[x].geometry.coordinates = ol.proj.transform(t.features[x].geometry.coordinates, 'EPSG:27700', 'EPSG:3857');
										break;
									}								
								} 
							}						
							t.crs.properties.name='EPSG:3857';
							featureprj='EPSG:3857';
						}
						else{
							t.crs.properties.name='EPSG:27700';
							featureprj='EPSG:27700';
						}
						if(highlightLayer != null){
							map.removeLayer(highlightLayer);
						}
						
						highlightLayer = new ol.layer.Vector({
							source: new ol.source.Vector({
								format: new ol.format.GeoJSON({
									defaultDataProjection: featureprj,
									featureProjection: featureprj
								}),
								features: (new ol.format.GeoJSON()).readFeatures(t)
							}),
							style: highlightStyle
						});						
						highlightLayer.setVisible(true);						
						map.addLayer(highlightLayer);
						$(list2).jqGrid("GridUnload");						
						//$('#tableGrid').show();
						showGridData(t,"Selected Features");
					}
				}
			}
		}				
	);
}
// For BNG graticule, transformation between EPSG:4326 and EPSG:27700 is needed 
// so proj4 support is required

proj4.defs("EPSG:27700", '+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717' +
    ' +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs');

/* limit the bounds of the UK projection for the graticule */
ol.proj.get('EPSG:27700').setExtent([0, 0, 800000, 1400000]);
ol.proj.get('EPSG:27700').setWorldExtent([-10, 49, 6, 63]);
ol.proj.get('EPSG:27700').setGlobal(false);
ol.proj.get('EPSG:27700').setGetPointResolution(function (resolution) { return resolution; });

/* Simple example of OS Map + Gazetteer and Postcode lookup, all coordinates in EPSG:27700 */

/* to restrict the set of layers avalable use e.g. ["50KR", "50K", "SVR", "SV"] as the final constructor parameter */
/* See the OpenSpaceOl3 source code for a list of available layers */

var openSpaceOl3 = new OpenSpaceOl3('3AE71B01F7034829E0530C6CA40A35B3', "https://geosolutions.cognizant.com/", OpenSpaceOl3.ALL_LAYERS);

var llMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.toStringHDMS,
    projection: 'EPSG:4326',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-ll'),
    undefinedHTML: '&nbsp;'
});

var bngMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(0),
    projection: 'EPSG:27700',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-bng'),
    undefinedHTML: '&nbsp;'
});

var webMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(0),
    projection: 'EPSG:3857',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-web'),
    undefinedHTML: '&nbsp;'
});


var map = null;
var gmap = null;
var baseMapType = "OSM";

setMap();




function setMap() {


    var mapDetails = sessionStorage.getItem("mapDetails");
    if (mapDetails != undefined && mapDetails != null) {
        var json = JSON.parse(mapDetails);
        baseMapType = json.baseMapType;
    }


    var olMapDiv = document.getElementById('olmap');
    if (map !== null) {
        removeAllLayers();        
    }
    var lyrs = null;
    if (baseMapType == "Roads") {
        getBaseMapLayer(baseMapType);
        lyrs = [setAWGISData()];
    }
    
    else {
        lyrs = [getBaseMapLayer(baseMapType),
           setAWGISData()];
    }

    map = new ol.Map({
        layers: lyrs,
        logo: false,
        target: olMapDiv,
        controls: ol.control.defaults({
            attributionOptions: ({
                collapsible: false
            })
        }),
        view: getView(baseMapType)
    });

    if (gmap != null) {
        olMapDiv.parentNode.removeChild(olMapDiv);
        gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(olMapDiv);
    }

    function getView(baseMapType) {
        if (baseMapType == "OSM") {
            return new ol.View({
                projection: openSpaceOl3.getProjection(),
                center: [588090.0000000002, 187199.99999999997], // OS coords
                resolutions: openSpaceOl3.getResolutions(),
                resolution: 2,
                //extent: ol.proj.get('EPSG:27700').getExtent()
            });
        }
        else {
            return new ol.View({
                center: [153882.24729410993, 6935747.320588947],
                zoom: 5,
              
            });
        }
    }

    function getBaseMapLayer(type) {
        if (type == "OSM")
            return openSpaceOl3.getLayer();
        if (type == "Roads")
            return getGoogleBaseMapLayer(baseMapType);
    };

   
    
    function getGoogleBaseMapLayer(layerType) {
        

        gmap = new google.maps.Map(document.getElementById('gmap'), {
            disableDefaultUI: true,
            keyboardShortcuts: false,
            draggable: false,
            disableDoubleClickZoom: true,
            scrollwheel: false,
            streetViewControl: false
        });

        
        
    };

    function setAWGISData() {
        return new ol.layer.Group({
            title: 'Overlays',
            layers: [
                new ol.layer.Image({
                    title: 'UK Border',
                    source: new ol.source.ImageWMS({
                        url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK.qgs',
                        params: { 'LAYERS': 'Border' },
                        serverType: 'qgis'
                    })
                }),
                new ol.layer.Image({
                    title: 'Sewer',
                    source: new ol.source.ImageWMS({
                        url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
                        params: { 'LAYERS': 'Sewer' },
                        serverType: 'qgis'
                    })
                }),
                new ol.layer.Image({
                    title: 'Manhole',
                    source: new ol.source.ImageWMS({
                        url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
                        params: { 'LAYERS': 'Manhole' },
                        serverType: 'qgis'
                    })
                }),
                new ol.layer.Image({
                    title: 'Hydrant',
                    source: new ol.source.ImageWMS({
                        url: 'https:://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
                        params: { 'LAYERS': 'Hydrant' },
                        serverType: 'qgis'
                    })
                })
            ]
        })
    }


    var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Legend' // Optional label for button
    });
    map.addControl(layerSwitcher);

    var identifyFeature = new ol.control.IdentifyFeature({
        tipLabel: 'Identify' // Optional label for button
    });
    map.addControl(identifyFeature);

    var baseMapSwitcher = new ol.control.BaseMapSwitcher({
        tipLabel: 'Base Map' // Optional label for button
    });
    map.addControl(baseMapSwitcher);
    baseMapSwitcher.hidePanel();

}



// Create the graticule component
var graticuleWeb = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(255,24,24,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    projection: 'EPSG:3857'
});

var graticuleLl = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(24,192,24,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    projection: 'EPSG:4326',
    intervals: [2, 1,
  30 / 60, 20 / 60, 10 / 60, 5 / 60, 3 / 60, 2 / 60, 1 / 60,
  30 / 3600, 20 / 3600, 10 / 3600, 5 / 3600, 3 / 3600, 2 / 3600, 1 / 3600]
});

var graticuleBng = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(24,24,255,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    intervals: [200000, 100000, 50000,
      20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10],
    projection: 'EPSG:27700'
});

function toggleLl(b) {
    if (b) {
        graticuleLl.setMap(map);

        setTimeout(function () {
            var secs = Math.round(3600 * graticuleLl.getInterval());
            document.getElementById('llInterval').innerHTML = ' Interval ' +
            Math.floor(secs / 3600) + '\u00b0 ' +
            Math.floor((secs / 60) % 60) + '\u2032 ' +
            Math.floor(secs % 60) + '\u2033 '
        }, 100);

    }
    else {
        graticuleLl.setMap(null);
    }
}

function toggleBng(b) {
    if (b) {
        graticuleBng.setMap(map);
        setTimeout(function () { document.getElementById('bngInterval').innerHTML = ' Interval ' + graticuleBng.getInterval() + ' metres'; }, 100);
    }
    else {
        graticuleBng.setMap(null);
    }
}

function toggleWeb(b) {
    if (b) {
        graticuleWeb.setMap(map);
        setTimeout(function () { document.getElementById('webInterval').innerHTML = ' Interval ' + graticuleWeb.getInterval() + ' metres'; }, 100);
    }
    else {
        graticuleWeb.setMap(null);

    }
}

map.on('moveend', function (evt) {

    //    var secs = Math.round(3600 * graticuleLl.getInterval());
    //document.getElementById('llInterval').innerHTML = ' Interval ' + 
    //Math.floor(secs / 3600) + '\u00b0 ' +
    //Math.floor((secs / 60) % 60) + '\u2032 ' +
    //Math.floor(secs % 60) + '\u2033 ';

    //document.getElementById('bngInterval').innerHTML = ' Interval ' + graticuleBng.getInterval() + ' metres';
    //document.getElementById('webInterval').innerHTML = ' Interval ' + graticuleWeb.getInterval() + ' metres';
});


document.addEventListener("basemapChanged", function (e) {
    switch (e.detail.mapSelected) {
        case "Roads":
            baseMapType = "Roads";
            setLocalStorage(baseMapType);
            break;
        case "OS Map":
            baseMapType = "OSM";
            setLocalStorage(baseMapType);
        default:
            break;
    }
});



function setLocalStorage(baseMapType) {
    var mapDetails = { baseMapType : baseMapType}
    sessionStorage.setItem("mapDetails", JSON.stringify(mapDetails));
    location.reload();
    
}
var gridhighlightLayer;
function CreationOfGridaData(urlName, legenLabel) {
    var list2 = document.getElementById('list2');
    var pager2 = document.getElementById('pager2');
    var data;
    var result = [];
    var resultstring;
    var colmodelstring = {};
    var test = [];
    var gridData;
    $.ajax({
        url: urlName,
        dataType: 'application/json',
        success: function(r) {
            console.log(r);
        },
        statusCode: {
            200: function(r) {
                var x;
                eval('x=' + r.responseText);  
				//$('#tableGrid').show();
				showGridData(x,legenLabel);
                //console.log(r.responseText);
            }
        }
    });    
}

function getNames(obj, name) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) {
			if ("object" == typeof (obj[key])) {
				getNames(obj[key], name);
			} else if (key == name) {
				result.push(obj[key]);
			}
		}
	}
}
	
function showGridData(x,legenLabel){
	var k = x;
    data = x.features;
	var propertiesObject = data[0].properties;
	var test = [];
	var colnames = [];
	var geometryObject = data[0].geometry.coordinates;
	for (var propt in propertiesObject) {
		resultstring = {};
		resultstring['name'] = propt;
		resultstring['index'] = propt;
		resultstring['width'] = 40;
		resultstring['align'] = 'center';
		resultstring['sortable'] = false;
		////resultstring =  '{name:'+ "'" + propt + "'" +',index:' + "'" + propt + "'" + ', width:60, align:"center"}'//"'" + propt + "'" //  +',index:' + propt + ', width:60, align:"center"}'
		//resultstring +=',{name:'+ "'" + propt + "'" +',index:' + "'" + propt + "'" + ', width:60, align:"center"}'
		test.push(resultstring);
		colnames.push(propt);
	}
	var keyLength = Object.keys(geometryObject).length

	//test = JSON.stringify(resultstring);
	console.log(test);
	jQuery(list2).jqGrid({
		datatype: "local",
		height: 125,
		//width:	'100px', //map.getView().calculateExtent(map.getSize()), //'20px',//
		//autowidth: true,
		pager: '#pager2',
		//set your pager div id
		search: true,
		multiselect: true,
		multiboxonly: true,
		colNames: colnames,
		colModel: //[{name:'AQU_METHOD',index:'AQU_METHOD',  width:60, align:"center", sortable:false},
		//{name:'ASSET_NO',index:'ASSET_NO', width:90, sorttype:"int", width:22, align:"center", sortable:false}
		test,
		//]
		width: "1300",
		multiboxonly: true,
		caption: legenLabel,
		//"Grid Data",
	});
	//$(list2).jqGrid("clearGridData");
	//$(list2).trigger('reloadGrid');

	$(list2).jqGrid('navGrid', '#pager2', {
		add: false,
		edit: false,
		del: false,
		search: true,
		refresh: true
	}, {}, {}, {}, {
		multipleSearch: true,
		multipleGroup: true,
		showQuery: true
	});

	$(list2).jqGrid('navGrid', '#pager2', {
		edit: false,
		add: false,
		del: false,
		search: false,
		pdf: true,
	}, {}, {}, {}, {});

	$(list2).jqGrid('navButtonAdd', '#pager2', {
		id: 'ExportToExcel',
		caption: 'Export Selected Rows To CSV',
		title: 'Export Selected Rows To CSV',
		onClickButton: function(e) {
			exportData(e, list2);
		},
		buttonicon: 'ui-icon ui-icon-document',
	});

	$(list2).jqGrid('navButtonAdd', '#pager2', {
		id: 'HighlightOnMap',
		caption: 'Highlight On Map',
		title: 'Highlight On Map',
		onClickButton: function(e) {
			highlightOnMap(e, list2);
		},
		buttonicon: 'ui-icon ui-icon-document',
	});

	$(list2).jqGrid('navButtonAdd', '#pager2', {
		id: 'ClearOnMap',
		caption: 'Clear features On Map',
		title: 'Clear features On Map',
		onClickButton: function(e) {
			ClearMap(e, list2);
		},
		buttonicon: 'ui-icon ui-icon-document',
	});
	for (var i = 0; i < data.length; i++) {
		gridData = {};
		/* for(var geoObjcord in data[i].geometry.coordinates)
{
gridData[geoObjcord] = data[i].geometry.coordinates[geoObjcord];
} */

		for (var griDataProp in data[i].properties) {
			gridData[griDataProp] = data[i].properties[griDataProp];
		}
		jQuery(list2).jqGrid('addRowData', i + 1, gridData);
		//data[i].properties);
		// newData[newData.length-i-1],
	}
	$(list2).jqGrid('setGridParam', {
		ondblClickRow: function(rowid, iRow, iCol, e) {
			alert('double clicked');
		}
	});

	var myVectorLayer;
	function highlightOnMap(e, id) {
		var defaultDataProjectionNumber;
		var featureProjectionNumber;
		var gridIDforMap = jQuery(id).getDataIDs();
		var labelforMap = jQuery(id).getRowData(gridIDforMap[0]);
		var selRowIdsforMap = jQuery(id).jqGrid('getGridParam', 'selarrrow');
		var objforMap = new Object();
		objforMap.count = selRowIdsforMap.length;
		var t = jQuery.extend(true, {},k);
		if (objforMap.count) {
			objforMap.items = new Array();
			for (elem in selRowIdsforMap) {
				objforMap.items.push(data[parseInt(selRowIdsforMap[elem] - 1)]);
				//objforMap.items.push(jQuery(id).getRowData( selRowIdsforMap[elem] ));
			}
			t.features = jQuery.extend(true, {},objforMap.items);
			t.totalFeatures = objforMap.items.length;
			t.features.length = t.totalFeatures;
				var extentToBeSHownLineString = [];
				if (t.features && t.features.length > 0) {
					if(googleInitialized)
					{
						for (var x = 0; x < t.features.length; x++) {
							if (data[0].geometry.type == 'Point') {
									t.features[x].geometry.coordinates = ol.proj.transform(t.features[x].geometry.coordinates, 'EPSG:27700', 'EPSG:3857');
							} 
							else {
								for (var y = 0; y < t.features[x].geometry.coordinates.length; y++) {
									t.features[x].geometry.coordinates[y] = ol.proj.transform(t.features[x].geometry.coordinates[y], 'EPSG:27700', 'EPSG:3857');
								}
								extentToBeSHownLineString.push( t.features[x].geometry.coordinates);
							}
						}
						t.crs.properties.name = 'EPSG:3857';
						defaultDataProjectionNumber = 'EPSG:3857';
						featureProjectionNumber = 'EPSG:3857';
					}
					else
					{
						defaultDataProjectionNumber = 'EPSG:27700';
						featureProjectionNumber = 'EPSG:27700';
					}
					if (gridhighlightLayer) {
						map.removeLayer(gridhighlightLayer);
					}
					gridhighlightLayer = new ol.layer.Vector({
						source: new ol.source.Vector({
							format: new ol.format.GeoJSON({
								defaultDataProjection: defaultDataProjectionNumber,
								featureProjection: featureProjectionNumber
							}),
							features: (new ol.format.GeoJSON()).readFeatures(t)
						}),
						style: highlightStyle
					});
					gridhighlightLayer.setVisible(true);
					map.addLayer(gridhighlightLayer);
					map.getView().fit(gridhighlightLayer.getSource().getExtent(), map.getSize());
					//setContent(t.features);
					 // var ext = ol.extent.boundingExtent(extentToBeSHownLineString);
				// ext = ol.proj.transformExtent(ext, ol.proj.get('EPSG:27700'), ol.proj.get('EPSG:3857'));
				// map.getView().fit(ext, map.getSize());
					//console.log(t.features);
				}
			}
		

	}

	function ClearMap(e, list2) {
		if(gridhighlightLayer){
			map.removeLayer(gridhighlightLayer);
			gridhighlightLayer =null;
		}
		GetExtent();
	}

	function exportData(e, id) {
		var gridid = jQuery(id).getDataIDs();
		// Get all the ids in array
		var label = jQuery(id).getRowData(gridid[0]);
		// Get First row to get the labels
		var selRowIds = jQuery(id).jqGrid('getGridParam', 'selarrrow');

		var obj = new Object();
		obj.count = selRowIds.length;
		if (obj.count) {
			obj.items = new Array();
			for (elem in selRowIds) {
				// if(selRowIds[elem] === "0" || selRowIds[elem] === "1" || selRowIds[elem] === "2")
				// {
				// continue;
				// }
				// else
				// {
				obj.items.push(jQuery(id).getRowData(selRowIds[elem]));
				//}
			}
			var json = JSON.stringify(obj);
			JSONToCSVConvertor(json, "csv", 1);
		}
	}

	function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {

		//If JSONData is not an object then JSON.parse will parse the JSON string in an Object
		var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
		var CSV = '';
		//This condition will generate the Label/Header
		if (ShowLabel) {
			var row = "";

			//This loop will extract the label from 1st index of on array
			for (var index in arrData.items[0]) {
				//Now convert each value to string and comma-seprated
				row += index + ',';
			}
			row = row.slice(0, -1);
			//append Label row with line break
			CSV += row + '\r\n';
		}

		//1st loop is to extract each row
		for (var i = 0; i < arrData.items.length; i++) {
			var row = "";
			//2nd loop will extract each column and convert it in string comma-seprated
			for (var index in arrData.items[i]) {
				row += '"' + arrData.items[i][index].replace(/(<([^>]+)>)/ig, '') + '",';
			}
			row.slice(0, row.length - 1);
			//add a line break after each row
			CSV += row + '\r\n';
		}

		if (CSV == '') {
			alert("Invalid data");
			return;
		}

		/*
* 
* FORCE DOWNLOAD
* 
*/
		//this trick will generate a temp "a" tag
		var link = document.createElement("a");
		link.id = "lnkDwnldLnk";

		//this part will append the anchor tag and remove it after automatic click
		document.body.appendChild(link);

		var csv = CSV;
		blob = new Blob([csv],{
			type: 'text/csv'
		});
		var myURL = window.URL || window.webkitURL;
		var csvUrl = myURL.createObjectURL(blob);
		var filename = 'UserExport.csv';
		jQuery("#lnkDwnldLnk").attr({
			'download': filename,
			'href': csvUrl
		});

		jQuery('#lnkDwnldLnk')[0].click();
		document.body.removeChild(link);
	}
}
//);
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
		if(this.checkQueryParams("logout") === null){
			this.receivedEvent('deviceready');
		}else{
			this.googleMapsLogout();
		}
        
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
		var mobileService = new WindowsAzure.MobileServiceClient('https://aws-mjb-mob-ppd-01.awis.mobi');
		mobileService.login('aad').done(function (results) {
			var userId = results.userId;
			//alert("user Details : " + userId);
			console.log("user Details : " + userId);
			window.location.href = "index.html";
		}, function (error) {
			alert("loginAzure FAIL - error = " + error );
			console.log(error)
		});
    },
	checkQueryParams: function(name){
		var url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	},
	googleMapsLogout: function(){
		
		var logoutUrl = "https://login.windows.net/common/oauth2/logout";
		var ref = cordova.InAppBrowser.open(logoutUrl, '_blank', 'hidden=yes,location=no,clearsessioncache=yes');

    ref.addEventListener('loadstop', function () {
        ref.close();
        console.log("SYNC:logoutazure ref.close");
    });

    console.log("SYNC:logoutazure calling mobileService.logout");
		
		
		var mobileService = new WindowsAzure.MobileServiceClient('https://aws-mjb-mob-ppd-01.awis.mobi');
		mobileService.logout(function () {
			console.log("loged out sucessfully");
		}, function () {
			console.log("SYNC:error Logging out of Azure");
		});
		
		
	}
};

app.initialize();
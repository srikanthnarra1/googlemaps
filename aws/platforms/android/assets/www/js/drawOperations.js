var polygon;
 var tempfeatureOverlay;
function CreateFeatures()

{
  var features = new ol.Collection();
   var featureOverlay= new ol.layer.Vector({
													source: new ol.source.Vector({features: features}),
													style: new ol.style.Style({
													  fill: new ol.style.Fill({
														color: 'rgba(255, 255, 255, 0.2)'
													  }),
													  stroke: new ol.style.Stroke({
														color: '#ffcc33',
														width: 2
													  }),
													  image: new ol.style.Circle({
														radius: 7,
														fill: new ol.style.Fill({
														  color: '#ffcc33'
														})
													  })
													})
												});
							featureOverlay.setMap(map);
							tempfeatureOverlay=featureOverlay;
 
							return features;
}

function addInteraction(evtType) {
	
	if(tempfeatureOverlay !=null)
	{
		tempfeatureOverlay.getSource().clear();
		
	}
										
										var draw = new ol.interaction.Draw({
																			features: CreateFeatures(),
																			type: /** @type {ol.geom.GeometryType} */ (evtType)
																			
																		});
										map.addInteraction(draw);
										typeSelect=$(('#' + evtType ));
										
										
										draw.on('drawend', function(event) {

																			polygon = event.feature.getGeometry();

																			$( "#dialog" ).dialog();
																			map.removeInteraction(draw);
																			
																	getFeatureInfo1(polygon);

																/*	var polygon_extent = polygon.getExtent();
																						map.getLayers().item(0).getSource().forEachFeatureIntersectingExtent(polygon_extent, function(feature) {
																						console.info(feature.ol_uid);
																	});*/

																			});

								}
  
function ClearGeometry()
{
	tempfeatureOverlay.getSource().clear();
  
}


function getFeatureInfo1(coordinate){
	var size = map.getSize();
	var view = map.getView();
	var viewResolution = view.getResolution();
	
	var layerNames=[];
	for(var x=0;x<layerData.data.length;x++){
		if(layerData.data[x].layers.length>0){
			for(var y=0;y<layerData.data[x].layers.length;y++){
				var data = layerData.data[x].layers[y];
				if(data.layer.getVisible()){
					layerNames.push(data.name);
				}
			}	
		}
	}
	
	/*var url = layerData.baseUrl+layerData.wmsUrl+'?'+layerData.featureInfoParams.join('&');
	url += '&WIDTH='+size[0]+'&HEIGHT='+size[1];+'&I='+coordinate[0]+'&J='+coordinate[1];
	url +='&CRS='+view.getProjection().getCode()+'&BBOX='+coordinate.getExtent().join(',');
	url +='&QUERY_LAYERS='+layerNames.join(',')+'&LAYERS='+layerNames.join(',')+'&FEATURE_COUNT=5&BUFFER=10';*/
	
	var url= layerData.baseUrl + "GEOSPATIAL/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=";
	url +=layerNames.join(',')+'&maxFeatures=50'; 
	url += "&outputFormat=application/json" + "&BBOX=" + coordinate.getExtent().join(',') + ',EPSG:3857';
	console.log(url);
	$.ajax({
			url: url,//+'&FI_POINT_TOLERANCE=16&FI_LINE_TOLERANCE=8&FI_POLYGON_TOLERANCE=4',	
			dataType:'application/json',
			success:function(r){
				console.log(r);
			},
			statusCode:{
				200:function(r){
					var t;
					eval('t='+r.responseText);					
					if(t.features && t.features.length>0){
						for(var x=0;x<t.features.length;x++){
							for(var y=0;y<t.features[x].geometry.coordinates.length;y++){
								t.features[x].geometry.coordinates[y] = ol.proj.transform(t.features[x].geometry.coordinates[y], 'EPSG:27700', 'EPSG:3857');
							}
						}
						t.crs.properties.name='EPSG:3857';
						if(highlightLayer != null){
							map.removeLayer(highlightLayer);
						}
						highlightLayer = new ol.layer.Vector({
							source: new ol.source.Vector({
								format: new ol.format.GeoJSON({
									defaultDataProjection: 'EPSG:3857',
									featureProjection: 'EPSG:3857'
								}),
								features: (new ol.format.GeoJSON()).readFeatures(t)
							}),
							//style: highlightStyle
							style: new ol.style.Style({
													  fill: new ol.style.Fill({
														color: 'rgba(255, 255, 255, 0.2)'
													  }),
													  stroke: new ol.style.Stroke({
														color: '#ffcc33',
														width: 2
													  }),
													  image: new ol.style.Circle({
														radius: 7,
														fill: new ol.style.Fill({
														  color: '#ffcc33'
														})
													  })
													})
						});
						highlightLayer.setVisible(true);
						map.addLayer(highlightLayer);						
						setContent(t.features);
						console.log(t.features);
					}
				}
			}
		}				
	);
}

var layerData = {
    baseUrl: 'https://geosolutions.cognizant.com/geoserver3/',
    vtUrl: 'gwc/service/tms/1.0.0/',
    wmsUrl: 'GEOSPATIAL/wms',
	featureInfoParams:[
		'SERVICE=WMS',
		'VERSION=1.3.0',
		'REQUEST=GetFeatureInfo',
		'FORMAT=image/png',
		'TRANSPARENT=true',
		'INFO_FORMAT=application/json',
		'STYLES=',
		'EXCEPTIONS=application/json'
	],
    data: [{
        group: 'Sewer', layers: [
            {
                name: 'GEOSPATIAL:SEWER',
                legendLabel: 'Sewer line',
				layer:null,
				visibility:true,
				style:function(feature) {
					return new ol.style.Style({
						fill: new ol.style.Fill({
							color: '#ADD8E6'
						}),
						stroke: new ol.style.Stroke({
							color: '#444444',
							width: 2
						})
					});
				}
            },
            {
                name: 'GEOSPATIAL:MANHOLE',
                legendLabel: 'Manhole',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						text: new ol.style.Text({
							fill: new ol.style.Fill({
								color: '#f30033'
							}),
							stroke: new ol.style.Stroke({
								color: '#f30033',
								width: 1
							}),
							font: '20px sewer',
							text:'\uf023'
						})
					});
				}
            },
            {
                name: 'GEOSPATIAL:SEWERVALVE',
                legendLabel: 'Sewer Valve',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						text: new ol.style.Text({
							fill: new ol.style.Fill({
								color: '#f30033'
							}),
							stroke: new ol.style.Stroke({
								color: '#f30033',
								width: 1
							}),
							font: '20px sewer',
							text: '\uf04d'
						})
					});
				}
            },
            {
                name: 'GEOSPATIAL:SEWAIRVALVE',
                legendLabel: 'Sewer Air Valve',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						text: new ol.style.Text({
							fill: new ol.style.Fill({
								color: '#f30033'
							}),
							stroke: new ol.style.Stroke({
								color: '#f30033',
								width: 1
							}),
							font: '20px sewer',
							text: '\uf04d'
						})
					});
				}
            }
        ]
    },
    {
        group: 'Water', layers: [
            {
                name: 'GEOSPATIAL:MAINSPIPE',
                legendLabel: 'Mainspipe',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						fill: new ol.style.Fill({
							color: '#ADD8E6'
						}),
						stroke: new ol.style.Stroke({
							color: '#4444ff',
							width: 2
						})
					});
				}
            },
            {
                name: 'GEOSPATIAL:HYDRANT',
                legendLabel: 'Hydrant',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						text: new ol.style.Text({
							fill: new ol.style.Fill({
								color: '#3300f3'
							}),
							stroke: new ol.style.Stroke({
								color: '#3300f3',
								width: 1
							}),
							font: '20px water',
							text: '\uf034'
						})
					});
				}
            },            
            {
                name: 'GEOSPATIAL:WATERVALVE',
                legendLabel: 'Water Valve',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						text: new ol.style.Text({
							fill: new ol.style.Fill({
								color: '#3300f3'
							}),
							stroke: new ol.style.Stroke({
								color: '#3300f3',
								width: 1
							}),
							font: '28px water',
							text: '\uf05A'
						})
					});
				}
            },
            {
                name: 'GEOSPATIAL:WATERAIRVALVE',
                legendLabel: 'Water Air Valve',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						text: new ol.style.Text({
							fill: new ol.style.Fill({
								color: '#3300f3'
							}),
							stroke: new ol.style.Stroke({
								color: '#3300f3',
								width: 1
							}),
							font: '24px water',
							text: '\uf053'
						})
					});
				}
            },
            {
                name: 'GEOSPATIAL:WATERJOINT',
                legendLabel: 'Water Joint',
				layer:null,
				visibility:false,
				style:function(feature) {
					return new ol.style.Style({
						fill: new ol.style.Fill({
							color: '#ADD8E6'
						}),
						stroke: new ol.style.Stroke({
							color: '#444444',
							width: 2
						})
					});
				}
            }
        ]
    }]
};

function createLayers(srs1, srs2){
                var layers =[];
                var layerCtrl ='';
                for(var x=0;x<layerData.data.length;x++){
                                if(layerData.data[x].layers.length>0){
                                                for(var y=0;y<layerData.data[x].layers.length;y++){
                                                                var data = layerData.data[x].layers[y];
                                                                if(srs2 == 'EPSG:900913'){
                                                                                data.layer = new ol.layer.VectorTile({
                                                                                                title: data.legendLabel,
                                                                                                style: data.style,
                                                                                                source: new ol.source.VectorTile({
                                                                                                                tilePixelRatio: 1, // oversampling when > 1
                                                                                                                tileGrid: ol.tilegrid.createXYZ({ maxZoom: 22 }),
                                                                                                                format: new ol.format.GeoJSON({
                                                                                                                                defaultDataProjection: srs1,
                                                                                                                                featureProjection: srs2
                                                                                                                }),                                                                                                                           
                                                                                                                url: layerData.baseUrl+layerData.vtUrl+data.name+'@'+srs1+'@geojson/{z}/{x}/{-y}.geojson'
                                                                                                })
                                                                                });
                                                                }
                                                                else{
                                                                                data.layer = new ol.layer.Image({
                                                                                                title: data.legendLabel,
                                                                                                style: data.style,
                                                                                                source: new ol.source.ImageWMS({
                                                                                                                url: layerData.baseUrl+layerData.wmsUrl,
                                                                                                                params: {'LAYERS': data.name},
                                                                                                                serverType: 'geoserver'
                                                                                                })
                                                                                });
                                                                }
                                                                
                                                                data.layer.setVisible(data.visibility);
                                                                var id='layer_'+x+'_'+y;
                                                                
                                                                if(data.visibility)
																{
                                                                                layerCtrl+='<li><div class="layer-label"><span>'+data.legendLabel+'</span></div><label class="switch"><input class="layer-on-off" id="'+id+'" checked="checked" type="checkbox" ><div class="slider round"></div> </label><div class="grid-btn"><img id="btnSearch" src="images/grid.png"></div></li>';
																	var urlName = layerData.baseUrl+layerData.vtUrl+data.name+'@'+srs1+'@geojson/16/32933/43853.geojson';
																	CreationOfGridaData (urlName);
																	
																}
                                                                else
                                                                                layerCtrl+='<li><div class="layer-label"><span>'+data.legendLabel+'</span></div><label class="switch"><input class="layer-on-off" id="'+id+'" type="checkbox" ><div class="slider round"></div></label><div class="grid-btn"><img id="btnSearch" src="images/grid.png"></div></li>';
                                                                layers.push(data.layer);                                                                                                                                                                
                                                }
                                }
                }
                document.getElementById('layer-list').innerHTML=layerCtrl;      
                $('.layer-on-off').change(function(){
                                var layerInfo = this.id.split('_');
                                var lyrData = layerData.data[parseInt(layerInfo[1])].layers[parseInt(layerInfo[2])];
                                lyrData.visibility = this.checked;
                                lyrData.layer.setVisible(this.checked);
                                console.log($(this).is(':checked'));
								
                });
				$('.grid-btn').click(function (e){
							 var layerInfos = this.id.split('_');
                                var lyrData = layerData.data[parseInt(layerInfos[1])].layers[parseInt(layerInfos[2])];
								var layrName = lyrData.name;
								//var urlforGrid = 
				});
                return layers;
}




/*---- Highlight Identified Feature ------------- */
var image = new ol.style.Circle({
  radius: 20,
  fill: null,
  stroke: new ol.style.Stroke({color: 'blue', width: 2})
});

var styles = {
  'Point': [new ol.style.Style({
    image: image
  })],
  'LineString': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 4
    })
  })],
  'MultiLineString': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 4
    })
  })],
  'MultiPoint': [new ol.style.Style({
    image: image
  })],
  'MultiPolygon': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 4
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)'
    })
  })],
  'Polygon': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      lineDash: [4],
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)'
    })
  })],
  'GeometryCollection': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 4
    }),
    fill: new ol.style.Fill({
      color: 'blue'
    }),
    image: new ol.style.Circle({
      radius: 10,
      fill: null,
      stroke: new ol.style.Stroke({
        color: 'blue'
      })
    })
  })],
  'Circle': [new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 2
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255,0,0,0.2)'
    })
  })]
};

var highlightStyle = function(feature, resolution) {
	return styles[feature.getGeometry().getType()];
};
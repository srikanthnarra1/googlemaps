ol.control.DrawFeature = function(opt_options) {

    var options = opt_options || {};
	var className = options.className !== undefined ? options.className : 'ol-draw';
	var this_ = this;
	this.features = new ol.Collection();
	this.featureOverlay = new ol.layer.Vector({
		source: new ol.source.Vector({features: this.features}),
		style: new ol.style.Style({
			fill: new ol.style.Fill({
				color: 'rgba(255, 255, 255, 0.1)'
			}),
			stroke: new ol.style.Stroke({
				color: '#ff3333',
				width: 3
			}),
			image: new ol.style.Circle({
				radius: 7,
				fill: new ol.style.Fill({
					color: '#ff3333'
				})
			})
		})
	});
	
    var pointButton = document.createElement('button');
	pointButton.className = className + '-point';
	pointButton.setAttribute('type', 'button');
	pointButton.title = 'Point';
	pointButton.appendChild(document.createTextNode(''));

	pointButton.onclick = function(e) {
        e = e || window.event;
        this_.addInteraction('Point');
        e.preventDefault();
    };

	var LineButton = document.createElement('button');
	LineButton.className = className + '-line';
	LineButton.setAttribute('type', 'button');
	LineButton.title = 'Line';
	LineButton.appendChild(document.createTextNode(''));
	
	LineButton.onclick = function(e) {
        e = e || window.event;
        this_.addInteraction('LineString');
        e.preventDefault();
    };
	
	var RectangleButton = document.createElement('button');
	RectangleButton.className = className + '-rectangle';
	RectangleButton.setAttribute('type', 'button');
	RectangleButton.title = 'Rectangle';
	RectangleButton.appendChild(document.createTextNode(''));

	RectangleButton.onclick = function(e) {
        e = e || window.event;
        this_.addInteraction('Rectangle');
        e.preventDefault();
    };

	var CircleButton = document.createElement('button');
	CircleButton.className = className + '-circle';
	CircleButton.setAttribute('type', 'button');
	CircleButton.title = 'Circle';
	CircleButton.appendChild(document.createTextNode(''));

	CircleButton.onclick = function(e) {
        e = e || window.event;
        this_.addInteraction('Circle');
        e.preventDefault();
    };
	  
	var PolygonButton = document.createElement('button');
	PolygonButton.className = className + '-polygon';
	PolygonButton.setAttribute('type', 'button');
	PolygonButton.title = 'Polygon';
	PolygonButton.appendChild(document.createTextNode(''));

	PolygonButton.onclick = function(e) {
        e = e || window.event;
        this_.addInteraction('Polygon');
        e.preventDefault();
    };	  
	  
	var cssClasses = className + ' ol-unselectable ol-control';
	var element = document.createElement('div');
	element.className = cssClasses;
	element.appendChild(pointButton);
	element.appendChild(LineButton);
	element.appendChild(RectangleButton);
	element.appendChild(CircleButton);
	element.appendChild(PolygonButton);
	
    ol.control.Control.call(this, {
        element: element,
        target: options.target
    });

};

ol.inherits(ol.control.DrawFeature, ol.control.Control);

ol.control.DrawFeature.prototype.addInteraction = function(geometry){
	if(this.draw){
		this.getMap().removeInteraction(this.draw);
	}
	if(identifyFeature){
		identifyFeature.deactivateControl();
	}
	if(geometry == 'Rectangle'){
		this.draw = new ol.interaction.Draw({
			features: this.features,
			type: 'Circle',
			geometryFunction: ol.interaction.Draw.createBox()
		});
	}
	else{
		this.draw = new ol.interaction.Draw({
			features: this.features,
			type: geometry
		});
	}
	this.getMap().addInteraction(this.draw);
};


ol.control.DrawFeature.prototype.editInteraction = function(){
	this.edit = new ol.interaction.Modify({
	  features: this.features,
	  // the SHIFT key must be pressed to delete vertices, so
	  // that new vertices can be drawn at the same position
	  // of existing vertices
	  deleteCondition: function(event) {
		return ol.events.condition.shiftKeyOnly(event) &&
			ol.events.condition.singleClick(event);
	  }
	});
	this.getMap().addInteraction(this.edit);
};

/**
 * Set the map instance the control is associated with.
 * @param {ol.Map} map The map instance.
 */
ol.control.DrawFeature.prototype.setMap = function(map) {

    // Wire up listeners etc. and store reference to new map
    ol.control.Control.prototype.setMap.call(this, map);
    if (map) {        
		this.featureOverlay.setMap(map);
    }
};
/**
 * Generate a UUID
 * @returns {String} UUID
 *
 * Adapted from http://stackoverflow.com/a/2117523/526860
 */
ol.control.DrawFeature.uuid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

/**
* @private
* @desc Apply workaround to enable scrolling of overflowing content within an
* element. Adapted from https://gist.github.com/chrismbarr/4107472
*/
ol.control.DrawFeature.enableTouchScroll_ = function(elm) {
   if(ol.control.DrawFeature.isTouchDevice_()){
       var scrollStartPos = 0;
       elm.addEventListener("touchstart", function(event) {
           scrollStartPos = this.scrollTop + event.touches[0].pageY;
       }, false);
       elm.addEventListener("touchmove", function(event) {
           this.scrollTop = scrollStartPos - event.touches[0].pageY;
       }, false);
   }
};

/**
 * @private
 * @desc Determine if the current browser supports touch events. Adapted from
 * https://gist.github.com/chrismbarr/4107472
 */
ol.control.DrawFeature.isTouchDevice_ = function() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch(e) {
        return false;
    }
};
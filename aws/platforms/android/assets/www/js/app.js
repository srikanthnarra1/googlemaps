var map, gmap, view, overLayLayers = [], baseMapLayers = [];
var highlightLayer = null;
var currentCenter, zoom;
var DEFAULT_BASE_MAP = 0;
var googleInitialized = false;
var osmInitialized = false;
var extentVariable= null;
// For BNG graticule, transformation between EPSG:4326 and EPSG:27700 is needed
// so proj4 support is required

proj4.defs("EPSG:27700", '+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717' +
    ' +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs');

/* limit the bounds of the UK projection for the graticule */
ol.proj.get('EPSG:27700').setExtent([0, 0, 800000, 1400000]);
ol.proj.get('EPSG:27700').setWorldExtent([-10, 49, 6, 63]);
ol.proj.get('EPSG:27700').setGlobal(false);
ol.proj.get('EPSG:27700').setGetPointResolution(function (resolution) { return resolution; });

/* Simple example of OS Map + Gazetteer and Postcode lookup, all coordinates in EPSG:27700 */

/* to restrict the set of layers avalable use e.g. ["50KR", "50K", "SVR", "SV"] as the final constructor parameter */
/* See the OpenSpaceOl3 source code for a list of available layers */

var openSpaceOl3 = new OpenSpaceOl3('3AE71B01F7034829E0530C6CA40A35B3', "https://geosolutions.cognizant.com/", OpenSpaceOl3.ALL_LAYERS);

var llMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.toStringHDMS,
    projection: 'EPSG:4326',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-ll'),
    undefinedHTML: '&nbsp;'
});

var bngMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(0),
    projection: 'EPSG:27700',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-bng'),
    undefinedHTML: '&nbsp;'
});

var webMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(0),
    projection: 'EPSG:3857',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-web'),
    undefinedHTML: '&nbsp;'
});

// Create the graticule component
var graticuleWeb = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(255,24,24,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    projection: 'EPSG:3857'
});

var graticuleLl = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(24,192,24,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    projection: 'EPSG:4326',
    intervals: [2, 1,
  30 / 60, 20 / 60, 10 / 60, 5 / 60, 3 / 60, 2 / 60, 1 / 60,
  30 / 3600, 20 / 3600, 10 / 3600, 5 / 3600, 3 / 3600, 2 / 3600, 1 / 3600]
});

var graticuleBng = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(24,24,255,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    intervals: [200000, 100000, 50000,
      20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10],
    projection: 'EPSG:27700'
});

function toggleLl(b) {
    if (b) {
        graticuleLl.setMap(map);

        setTimeout(function () {
            var secs = Math.round(3600 * graticuleLl.getInterval());
            document.getElementById('llInterval').innerHTML = ' Interval ' +
            Math.floor(secs / 3600) + '\u00b0 ' +
            Math.floor((secs / 60) % 60) + '\u2032 ' +
            Math.floor(secs % 60) + '\u2033 '
        }, 100);

    }
    else {
        graticuleLl.setMap(null);
    }
}

function toggleBng(b) {
    if (b) {
        graticuleBng.setMap(map);
        setTimeout(function () { document.getElementById('bngInterval').innerHTML = ' Interval ' + graticuleBng.getInterval() + ' metres'; }, 100);
    }
    else {
        graticuleBng.setMap(null);
    }
}

function toggleWeb(b) {
    if (b) {
        graticuleWeb.setMap(map);
        setTimeout(function () { document.getElementById('webInterval').innerHTML = ' Interval ' + graticuleWeb.getInterval() + ' metres'; }, 100);
    }
    else {
        graticuleWeb.setMap(null);

    }
}

function viewGoogleMap(mapType) {
    if (!googleInitialized) {
        var viewExtent, mapSize;
        if (view) {
            var viewCurrent = view.calculateExtent(map.getSize());
            var min = ol.proj.transform([viewCurrent[0], viewCurrent[1]], 'EPSG:27700', 'EPSG:3857');
            var max = ol.proj.transform([viewCurrent[2], viewCurrent[3]], 'EPSG:27700', 'EPSG:3857');
            viewExtent = [min[0], min[1], max[0], max[1]];
            mapSize = map.getSize();
        }

        osmInitialized = false;
        googleInitialized = true;
        document.getElementById('map').innerHTML = '<div id="gmap"></div><div id="olmap"></div>';
        gmap = new google.maps.Map(document.getElementById('gmap'), {
            disableDefaultUI: true,
            keyboardShortcuts: false,
            draggable: false,
            disableDoubleClickZoom: true,
            scrollwheel: false,
            streetViewControl: true,
            mapTypeId: mapType
        });

        view = new ol.View({
            maxZoom: 21,
            minZoom: 2,
            extent: viewExtent
        });
        view.on('change:center', function () {
            var center = ol.proj.transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
            gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
        });
        view.on('change:resolution', function () {
            gmap.setZoom(view.getZoom());
        });
        var olMapDiv = document.getElementById('olmap');
        map = new ol.Map({
            layers: createLayers('EPSG:900913','EPSG:900913'),
            interactions: ol.interaction.defaults({
                altShiftDragRotate: false,
                dragPan: false,
                rotate: false
            }).extend([new ol.interaction.DragPan({ kinetic: null })]),
            target: olMapDiv,
            view: view
        });        
        addControl();

        if (!viewExtent) {
            view.setCenter(ol.proj.transform([0.9148752764892579, 51.88420015576608], 'EPSG:4326', 'EPSG:3857'));
            view.setZoom(16);
        }
        else {
            view.fit(viewExtent, mapSize);
        }

        olMapDiv.parentNode.removeChild(olMapDiv);
        gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(olMapDiv);
        map.render();
    }
    else {
		$(".basemapGalleryThumbnail" ).toggleClass( "selected" );
        gmap.setOptions({
            mapTypeId: mapType
        });
    }
}
var currentZoomLevel;



function checknewzoom(evt)
{
	
   var newZoomLevel = map.getView().getZoom();
   if (newZoomLevel != currentZoomLevel)
   {
      currentZoomLevel = newZoomLevel;
      $(document).trigger("zoomend", zoomend_event);
   }
}

$(document).on('zoomend', function () {
   console.log("Zoom");
  
   //Your code here
});
function viewOSBaseMap() {
    if (!osmInitialized) {
        googleInitialized = false;
        osmInitialized = true;
        document.getElementById('map').innerHTML = '';
        view = new ol.View({
            projection: openSpaceOl3.getProjection(),
            center: ol.proj.transform(view.getCenter(), 'EPSG:3857', 'EPSG:27700'), // OS coords
            resolutions: openSpaceOl3.getResolutions(),
            resolution: Math.round(view.getResolution()),
            extent: ol.proj.get('EPSG:27700').getExtent()
        });
        map = new ol.Map({
            layers: [openSpaceOl3.getLayer(),new ol.layer.Group({layers:createLayers('EPSG:900913','EPSG:27700')})],//createLayers('EPSG:900913','EPSG:27700'),
            logo: false,
            target: 'map',
            view: view
        });
        addControl();
               if(extentVariable == null)
			extentVariable = map.getView().calculateExtent(map.getSize()); 
    }
}

var clickedLocation;
function identifyFeature(){
	map.on('singleclick', identifyListener);
}


function logout() {
	window.location.href = "login.html?logout=yes";
}

function showMyLocation(){
			if (navigator.geolocation){
				 navigator.geolocation.getCurrentPosition(showPosition,onError);
			}
			else {alert( "Geolocation is not supported in the device."); }
	}
function showPosition(position) {
	if(googleInitialized){
		view.setCenter(ol.proj.transform([position.coords.longitude,position.coords.latitude], 'EPSG:4326', 'EPSG:3857'));
		view.setZoom(18);
		  var marker = new google.maps.Marker({
			position: { lat: position.coords.latitude, lng: position.coords.longitude },
			map: gmap              
		  });
		  map.setView(view);
	}else{
		view.setCenter(ol.proj.transform([position.coords.longitude,position.coords.latitude], 'EPSG:3857', 'EPSG:27700'));
		view.setZoom(12);
		/*var markers = new OpenLayers.Layer.Markers("Markers");
		map.addLayer(markers);
		var pos = new OpenSpace.MapPoint(position.coords.longitude,position.coords.latitude);
		var marker = new OpenLayers.Marker(pos);
		markers.addMarker(marker);*/
	}
		  
		  
}	
function onError(err){
		switch(error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out. Please try again");
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred. Please try again");
            break;
    }
}

function showLatLongPanal(){
	$('#gotoxypopup').show();
}


function zoomToLatLong(){
	map.getView().setCenter(ol.proj.transform([document.getElementById("long").value, document.getElementById("lat").value], 'EPSG:4326', 'EPSG:3857'));
    map.getView().setZoom(5);
}


var identifyListener =function(evt) {
	clickedLocation = evt.pixel;
	getFeatureInfo(evt.pixel);
	evt.stopPropagation();
};
function getFeatureInfo(coordinate){
	var size = map.getSize();
	var view = map.getView();
	var viewResolution = view.getResolution();
	
	var layerNames=[];
	for(var x=0;x<layerData.data.length;x++){
		if(layerData.data[x].layers.length>0){
			for(var y=0;y<layerData.data[x].layers.length;y++){
				var data = layerData.data[x].layers[y];
				if(data.layer.getVisible()){
					layerNames.push(data.name);
				}
			}	
		}
	}
	
	var url = layerData.baseUrl+layerData.wmsUrl+'?'+layerData.featureInfoParams.join('&');
	url += '&WIDTH='+size[0]+'&HEIGHT='+size[1]+'&I='+coordinate[0]+'&J='+coordinate[1];
	url +='&CRS='+view.getProjection().getCode()+'&BBOX='+view.calculateExtent(size).join(',');
	url +='&QUERY_LAYERS='+layerNames.join(',')+'&LAYERS='+layerNames.join(',')+'&FEATURE_COUNT=5&BUFFER=10';
	$.ajax({
			url: url,//+'&FI_POINT_TOLERANCE=16&FI_LINE_TOLERANCE=8&FI_POLYGON_TOLERANCE=4',	
			dataType:'application/json',
			success:function(r){
				console.log(r);
			},
			statusCode:{
				200:function(r){
					var t;
					eval('t='+r.responseText);					
					if(t.features && t.features.length>0){
						for(var x=0;x<t.features.length;x++){
							for(var y=0;y<t.features[x].geometry.coordinates.length;y++){
								t.features[x].geometry.coordinates[y] = ol.proj.transform(t.features[x].geometry.coordinates[y], 'EPSG:27700', 'EPSG:3857');
							}
						}
						t.crs.properties.name='EPSG:3857';
						if(highlightLayer != null){
							map.removeLayer(highlightLayer);
						}
						highlightLayer = new ol.layer.Vector({
							source: new ol.source.Vector({
								format: new ol.format.GeoJSON({
									defaultDataProjection: 'EPSG:3857',
									featureProjection: 'EPSG:3857'
								}),
								features: (new ol.format.GeoJSON()).readFeatures(t)
							}),
							style: highlightStyle
						});
						highlightLayer.setVisible(true);
						map.addLayer(highlightLayer);						
						setContent(t.features);
						console.log(t.features);
					}
				}
			}
		}				
	);
}
function closeIW(){
	if(highlightLayer != null){
		map.removeLayer(highlightLayer);
	}
	var iwContainer=document.getElementById("iw-container");
	iwContainer.style.display='none';
}
function setContent(data) {
	var iwContent=document.getElementById("iw-content");
	var iwContainer=document.getElementById("iw-container");
    iwContent.innerHTML='';
	var content="";
	for(var x=0;x<data.length;x++){
		if(data[x].id){
			content +="<div class='info-window-content header'>"+data[x].id+"</div><div class='hzLine'></div>";
			content +="<div><table class='attrTable' cellpadding='0px' cellspacing='0px'><tbody>";
			for(var y in data[x].properties){
				if(data[x].properties.hasOwnProperty(y)){
					//var attr = data[x].Layer.Feature.Attribute[y];
					var val = data[x].properties[y];
					if(!isNaN(val)){
						var tempVal = val*1;
						val = tempVal.toFixed(2);
					}
					content +="<tr valign='top'><td class='attrName'>"+y+"</td><td class='attrValue'>"+val+"</td></tr>";
				}
			}
			content +="</tbody></table></div>";
		}
	}
	iwContent.innerHTML=content;
	
	iwContainer.style.display='block';
	var h = iwContainer.clientHeight;
	var w = iwContainer.clientWidth;
	var mapSize = map.getSize();
	
	var vclassName="top";
	var hclassName="left";
	var anchorSize = 16;
	if (clickedLocation[1] < mapSize[1]/2) {
		iwContainer.style.top= (clickedLocation[1]+anchorSize)+"px";
    }
	else {
		iwContainer.style.top= (clickedLocation[1]-h-anchorSize)+"px";
		vclassName="bottom";
    }
	
	if (clickedLocation[0] < mapSize[0]/2 ) {
		iwContainer.style.left= (clickedLocation[0]+16)+"px";
    }
	else {
		iwContainer.style.left= (clickedLocation[0]-w-anchorSize)+"px";
		hclassName="right";
    }
}

function GetExtent() {
	if(googleInitialized){
	view.setCenter(ol.proj.transform([0.9148752764892579, 51.88420015576608], 'EPSG:4326', 'EPSG:3857'));
	view.setZoom(16);
	}
else
	{	
		view.setCenter(ol.proj.transform(extentVariable, 'EPSG:27700', 'EPSG:27700'));
	}
}

var drawFeature;
function addControl() {
    //var layerSwitcher = new ol.control.LayerSwitcher({
    //    tipLabel: 'Legend' // Optional label for button
    //});
    //map.addControl(layerSwitcher);

    /* identifyFeature = new ol.control.IdentifyFeature({
        tipLabel: 'Identify' // Optional label for button
    });
    map.addControl(identifyFeature);

    drawFeature = new ol.control.DrawFeature({
        tipLabel: 'Draw Feature' // Optional label for button
    });
    map.addControl(drawFeature); */
}
var marker;
$(document).ready(function() {

	$('#layerpopup').hide();
	if (DEFAULT_BASE_MAP == 0) {
		viewGoogleMap('roadmap');
		$("#address").autocomplete({
			minLength: 3,
			source: function (request, response) {
				$.getJSON({
					url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + request.term + '&key=AIzaSyDWppOeT1q71yTx3ac6k1_KttI8e_KdwFk',
					success: function (data, textStatus) {
						console.log(textStatus, data);
						var finalData = [];
						$.each(data.results, function (index, d) {
							var name = d.formatted_address;
							var obj = { label: name, value: name, geom: d.geometry };
							finalData.push(obj);
						});
						response(finalData);
					}
				});
			},
			select: function (event, ui) {  
				$("#clearSearch").show();
				var Lat = ui.item.geom.location.lat;
				var Lng = ui.item.geom.location.lng;
				view.setCenter(ol.proj.transform([Lng,Lat], 'EPSG:4326', 'EPSG:3857'));
				view.setZoom(18);
				if(marker)
					marker.setMap(null);
				marker = new google.maps.Marker({
				  position: {lat: Lat, lng: Lng},
				  map: gmap,
				  title: ui.item.value
				});
			}
		});
	}
/*	$("#clearSearch").click(function () {
		if(marker)
			marker.setMap(null);
		$("#address").val("");
		$(this).hide();
	});
	if ($(window).width() < 767) {
		$('#admin,#draw1,#draw2,#draw3,#draw4,#draw5,#close').hide();
		$("ul").removeClass("hover-color");
	}
	$('#search2').click(function () {
		if ($(window).width() < 400) {
			$('#export,#layers,#admin,#search2,#draw').hide();
			$("form").removeClass("hidden-xs");
			$('#searchbox,#searchicn').show();
		}
	});
	$('#closessrch').click(function () {
		if ($(window).width() < 400) {
			$("form").addClass("hidden-xs");
			$('#searchbox').hide();
			$('#export,#draw,#layers,#search2').show();
		}
	});
	$('#draw').click(function () {
		if ($(window).width() < 400) {
			$('#export,#layers,#admin,#search2,#draw').hide();
			$('#draw1,#draw2,#draw3,#draw4,#draw5,#close').show();
		}
	});
	$('#close').click(function () {
		if ($(window).width() < 400) {
			$("form").addClass("hidden-xs");
			$('#draw1,#draw2,#draw3,#draw4,#draw5,#close').hide();
			$('#export,#draw,#layers,#search2').show();
		}
	});*/
	// draw popup		
	$('#draw,#red_line').click(function () {
		$('#drawpopup,#redline').show();
	});
    $('#redline').click(function () {
		$('#redlinepopup').show();
	});
	$('#downLoad').click(function () {
       $('#layerpopup').slideUp();
       $('#RadioButton').show();
});

	$('#closedRadio').click(function() {
           $('#RadioButton').hide();
});

	$('#closedraw,#closeredline').click(function() {
		$('#drawpopup,#redlinepopup').hide();
	});
	// draw popup
	$('#layers').click(function () {
		map.un('singleclick',identifyListener);
		$('#layerpopup').slideDown();
	});
	$('#closelayer').click(function() {
		$('#layerpopup').slideUp();
	});
	
	// Measurement start
	$("#measurementpopup" ).draggable({ containment: "#map" });
	var measuringTool;
	vectorMeasurementLayer = new ol.layer.Vector({
	  source: new ol.source.Vector()
	});
	map.addLayer(vectorMeasurementLayer);
	var measuretypeSelect = document.getElementById('measuretype');
	var enableMeasuringTool = function() {
		if(measuretypeSelect.value === "0"){return;	}
		  map.removeInteraction(measuringTool);
		  var geometryType = (measuretypeSelect.value == 'Polygon' ? 'Polygon' : 'LineString');
		  var html = geometryType === 'Polygon' ? '<sup>2</sup>' : '';
		  measuringTool = new ol.interaction.Draw({
			type: geometryType,
			source: vectorMeasurementLayer.getSource()
		  });

		  measuringTool.on('drawstart', function(event) {
			vectorMeasurementLayer.getSource().clear();			
			event.feature.on('change', function(event) {
			  var measurement = geometryType === 'Polygon' ? event.target.getGeometry().getArea() : event.target.getGeometry().getLength();
			  var measurementFormatted = measurement > 100 ? (measurement / 1000).toFixed(2) + 'km' : measurement.toFixed(2) + 'm';
			  $('#lblmeasurementResult').html(measurementFormatted + html);
			});
		  });
		  
		  measuringTool.on('drawend', function (event) {
			// get the feature
			//var feature = event.feature;
			// ...listen for changes on it
			map.removeLayer(vectorMeasurementLayer);
			map.addLayer(vectorMeasurementLayer);
			console.log(event.feature.getGeometry().getCoordinates());
		  });

		  map.addInteraction(measuringTool);
	};
	
	measuretypeSelect.onchange = function(e) {
			if(measuretypeSelect.value !== "0") {
				enableMeasuringTool();
				vectorMeasurementLayer.getSource().clear();
				$('#lblmeasurementResult').html("");
			}else{
				map.removeInteraction(measuringTool);
				vectorMeasurementLayer.getSource().clear();
				$('#lblmeasurementResult').html("");
			}
		};
	
	$('#measurement').click(function () {
		$('#measurementpopup').show();
		enableMeasuringTool();
	});
	
	
	$('#closemeasurement').click(function() {
		$('#measurementpopup').hide();
		map.removeInteraction(measuringTool);
		vectorMeasurementLayer.getSource().clear();
		$('#lblmeasurementResult').html("");
	});
	//Measurement ends
	
});
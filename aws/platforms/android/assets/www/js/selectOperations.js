function SelectionOpp()
{
/* //////////// ADD SELECTION */

/* add ol.collection to hold all selected features */
var select = new ol.interaction.Select();
map.addInteraction(select);
var selectedFeatures = select.getFeatures();

/* //////////// ADD DRAWING */

/* The current drawing */
var sketch;

/* Add drawing vector source */
var drawingSource = new ol.source.Vector({
	useSpatialIndex : false
});


/* Add drawing layer */
var drawingLayer = new ol.layer.Vector({
	source: drawingSource
});
map.addLayer(drawingLayer);

/* Declare interactions and listener globally so we 
	can attach listeners to them later. */
var draw;
var modify;
var listener;

// Drawing interaction
draw = new ol.interaction.Draw({
	source : drawingSource,
	type : 'Polygon',
	//only draw when Ctrl is pressed.
	condition : ol.events.condition.platformModifierKeyOnly
});
map.addInteraction(draw);

/* Deactivate select and delete any existing polygons.
	Only one polygon drawn at a time. */
draw.on('drawstart',function(event){
	drawingSource.clear();
	//selectedFeatures.clear();
	select.setActive(false);
	
	sketch = event.feature;
	
	listener = sketch.getGeometry().on('change',function(event){
		selectedFeatures.clear();
		var polygon = event.target;
	//	var features = pointsLayer.getSource().getFeatures();

		/*for (var i = 0 ; i < features.length; i++){
			if(polygon.intersectsExtent(features[i].getGeometry().getExtent())){
				selectedFeatures.push(features[i]);
			}
		}*/
		
	});
},this);


draw.on('drawend', function(event) {
	sketch = null;
	delaySelectActivate();
	selectedFeatures.clear();

	var polygon = event.feature.getGeometry();
	//var features = pointsLayer.getSource().getFeatures();

	/*for (var i = 0 ; i < features.length; i++){
		if(polygon.intersectsExtent(features[i].getGeometry().getExtent())){
			selectedFeatures.push(features[i]);
		}
	}
	
	*/
});

}

function delaySelectActivate(){
	setTimeout(function(){
		select.setActive(true)
	},300);
}
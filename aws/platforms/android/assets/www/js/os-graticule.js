// For BNG graticule, transformation between EPSG:4326 and EPSG:27700 is needed 
// so proj4 support is required

proj4.defs("EPSG:27700", '+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717' +
    ' +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs');

/* limit the bounds of the UK projection for the graticule */
ol.proj.get('EPSG:27700').setExtent([0, 0, 800000, 1400000]);
ol.proj.get('EPSG:27700').setWorldExtent([-10, 49, 6, 63]);
ol.proj.get('EPSG:27700').setGlobal(false);
ol.proj.get('EPSG:27700').setGetPointResolution(function (resolution) { return resolution; });

/* Simple example of OS Map + Gazetteer and Postcode lookup, all coordinates in EPSG:27700 */

/* to restrict the set of layers avalable use e.g. ["50KR", "50K", "SVR", "SV"] as the final constructor parameter */
/* See the OpenSpaceOl3 source code for a list of available layers */

var openSpaceOl3 = new OpenSpaceOl3('3AE71B01F7034829E0530C6CA40A35B3', "https://geosolutions.cognizant.com/", OpenSpaceOl3.ALL_LAYERS);


var llMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.toStringHDMS,
    projection: 'EPSG:4326',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-ll'),
    undefinedHTML: '&nbsp;'
});

var bngMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(0),
    projection: 'EPSG:27700',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-bng'),
    undefinedHTML: '&nbsp;'
});

var webMousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(0),
    projection: 'EPSG:3857',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position-web'),
    undefinedHTML: '&nbsp;'
});
var olMapDiv = document.getElementById('olmap');
var map = new ol.Map({
    layers: [	
		new ol.layer.Group({
			'title': 'Base maps',
			layers: [
				new ol.layer.Group({
					title: 'Ordinance Survey',
					type: 'base',
					combine: true,
					visible: false,
					layers: [
						openSpaceOl3.getLayer()
					]
				}),
				new ol.layer.Group({
					title: 'Google Roads',
					type: 'base',
					combine: true,
					visible: true,
					layers:[]
				})
			]
		}),
		new ol.layer.Group({
			title: 'Overlays',
			layers: [
				new ol.layer.Image({
					title:'UK Border',
					source: new ol.source.ImageWMS({
						url: 'https://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK.qgs',
						params: { 'LAYERS': 'Border' },
						serverType: 'qgis'
					})
				}),
				new ol.layer.Image({
					title:'Sewer',
					source: new ol.source.ImageWMS({
						url: 'https://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
						params: { 'LAYERS': 'Sewer' },
						serverType: 'qgis'
					})
				}),
				new ol.layer.Image({
					title:'Manhole',
					source: new ol.source.ImageWMS({
						url: 'https://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
						params: { 'LAYERS': 'Manhole' },
						serverType: 'qgis'
					})
				}),
				new ol.layer.Image({
					title:'Hydrant',
					source: new ol.source.ImageWMS({
						url: 'https://geosolutions.cognizant.com/qgis/qgis_mapserv.fcgi.exe?map=C:\\OSGeo4W64\\QGISProjects\\UK_WATER.qgs',
						params: { 'LAYERS': 'Hydrant' },
						serverType: 'qgis'
					})
				})
			]
		})
	],
    logo: false,
    target: olMapDiv,
    controls: ol.control.defaults({
        attributionOptions: ({
            collapsible: false
        })
    }).extend([
    new OpenSpaceOl3.OpenSpaceLogoControl({ className: 'openspaceol3-openspace-logo' }),
    llMousePositionControl, bngMousePositionControl, webMousePositionControl
    ])
});

var osmView = new ol.View({
	projection: openSpaceOl3.getProjection(),
	center: [588090.0000000002,187199.99999999997], // OS coords
	resolutions: openSpaceOl3.getResolutions(),
	resolution: 2,
	extent: ol.proj.get('EPSG:27700').getExtent()
});

var gmView = new ol.View({    
    maxZoom: 21,      
});
gmView.on('change:center', function () {
    if (gmap != null) {
        var center = ol.proj.transform(gmView.getCenter(), 'EPSG:3857', 'EPSG:4326');
        gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
    }
});
gmView.on('change:resolution', function () {
    if (gmap != null) {
        gmap.setZoom(gmView.getZoom());
    }
});
var gmap = new google.maps.Map(document.getElementById('gmap'), {
	disableDefaultUI: true,
	keyboardShortcuts: false,
	draggable: false,
	disableDoubleClickZoom: true,
	scrollwheel: false,
	streetViewControl: true
});

var layerSwitcher = new ol.control.LayerSwitcher({
	tipLabel: 'Legend' // Optional label for button
});
map.addControl(layerSwitcher);

var identifyFeature = new ol.control.IdentifyFeature({
	tipLabel: 'Identify' // Optional label for button
});
map.addControl(identifyFeature);

// Create the graticule component
var graticuleWeb = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(255,24,24,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    projection: 'EPSG:3857'
});

var graticuleLl = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(24,192,24,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    projection: 'EPSG:4326',
    intervals: [2, 1,
  30 / 60, 20 / 60, 10 / 60, 5 / 60, 3 / 60, 2 / 60, 1 / 60,
  30 / 3600, 20 / 3600, 10 / 3600, 5 / 3600, 3 / 3600, 2 / 3600, 1 / 3600]
});

var graticuleBng = new ol.Graticule({
    // the style to use for the lines, optional.
    strokeStyle: new ol.style.Stroke({
        color: 'rgba(24,24,255,0.5)',
        width: 3,
        lineDash: [0.5, 4]
    }),
    intervals: [200000, 100000, 50000,
      20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10],
    projection: 'EPSG:27700'
});

function toggleLl(b) {
    if (b) {
        graticuleLl.setMap(map);

        setTimeout(function () {
        var secs = Math.round(3600 * graticuleLl.getInterval());
        document.getElementById('llInterval').innerHTML = ' Interval ' + 
        Math.floor(secs / 3600) + '\u00b0 ' +
        Math.floor((secs / 60) % 60) + '\u2032 ' +
        Math.floor(secs % 60) + '\u2033 '}, 100);

    }
    else {
        graticuleLl.setMap(null);
    }
}

function toggleBng(b) {
    if (b) {
        graticuleBng.setMap(map);
        setTimeout(function () { document.getElementById('bngInterval').innerHTML = ' Interval ' + graticuleBng.getInterval() + ' metres'; }, 100);
    }
    else {
        graticuleBng.setMap(null);
    }
}

function toggleWeb(b) {
    if (b) {
        graticuleWeb.setMap(map);
        setTimeout(function () { document.getElementById('webInterval').innerHTML = ' Interval ' + graticuleWeb.getInterval() + ' metres'; }, 100);
    }
    else {
        graticuleWeb.setMap(null);

    }
}

map.on('moveend', function (evt) {
});
